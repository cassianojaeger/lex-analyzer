/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 30/09/2020
*/
#include <stdio.h>
#include <stdlib.h>
#include "hash.h"
#include "ast.h"

int isRunning(void);
void initMe(void);
void hashPrint(void);
extern FILE *yyin;
int yyparse();

void astToFile(AST* node, FILE *fileTree);
void astPrint(AST *node, int level);
AST* getAST(void);

int main(int argc, char** argv)
{
    FILE *outputFile;

    fprintf(stderr,"Rodando main do prof. \n");

    if (argc < 2)
    {
        printf("call: ./etapa2 input.txt \n");
        exit(1);
    }
    if (0==(yyin = fopen(argv[1],"r")))
    {
        printf("Cannot open file %s... \n",argv[1]);
        exit(2);
    }
    if ((outputFile = fopen(argv[2], "w")) == 0)
    {
        fprintf(stderr, "Cannot open file %s\n", argv[1]);

        exit(4);
    }

    initMe();

    yyparse();

    astPrint(getAST(),0);
    //hashPrint();

    astToFile(getAST(), outputFile);
    fprintf(stderr,"\nSUCESSO!");
    exit(0);
}