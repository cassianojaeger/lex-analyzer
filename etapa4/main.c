/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/
#include <stdio.h>
#include <stdlib.h>
#include "hash.h"
#include "ast.h"
#include "semantic.h"

int isRunning(void);
void initMe(void);
void hashPrint(void);
extern FILE *yyin;
int yyparse();

void astToFile(AST* node, FILE *fileTree);
void astPrint(AST *node, int level);
AST* getAST(void);

int main(int argc, char** argv)
{
    fprintf(stderr,"Rodando main do prof. \n");

    if (argc < 1)
    {
        fprintf(stderr, "call: ./etapa4 input.txt \n");
        exit(1);
    }
    if (0==(yyin = fopen(argv[1],"r")))
    {
        fprintf(stderr, "Cannot open file %s... \n",argv[1]);
        exit(2);
    }

    initMe();
    yyparse();

    if(getSemanticErrors() > 0)
    {
        fprintf(stderr, "COMPILATION FAILED. SEMANTIC ERRORS\n");
        exit(4);
    }

    hashPrint();

    fprintf(stderr,"\n\nSUCCESSFUL COMPILATION!");
    exit(0);
}