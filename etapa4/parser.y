%{
/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/
	#include <stdio.h>
	#include <stdlib.h>
	#include "hash.h"
	#include "ast.h"
	#include "semantic.h"
	int yylex();
	int yyerror();
	int getLineNumber(void);

	AST *astRoot;
%}

%union
{
	HASH_NODE *symbol;
	AST *ast;
}

%token<ast> KW_CHAR
%token<ast> KW_INT
%token<ast> KW_FLOAT
%token<ast> KW_BOOL

%token KW_IF
%token KW_THEN
%token KW_ELSE
%token KW_WHILE
%token KW_LOOP
%token KW_READ
%token KW_PRINT
%token KW_RETURN

%token OPERATOR_LE
%token OPERATOR_GE
%token OPERATOR_EQ
%token OPERATOR_DIF

%token<symbol> TK_IDENTIFIER
%token<symbol> LIT_INTEGER
%token<symbol> LIT_FLOAT
%token<symbol> LIT_TRUE
%token<symbol> LIT_FALSE
%token<symbol> LIT_CHAR
%token<symbol> LIT_STRING

%type<ast> programa
%type<ast> declarationList
%type<ast> declaration
%type<ast> commandBlock
%type<ast> commandList
%type<ast> command

%type<ast> literalList
%type<ast> literal

%type<ast> varType
%type<ast> expr

%type<ast> callParameterList
%type<ast> restCallParater

%type<ast> parameterList
%type<ast> restParameter

%type<ast> printParameterList
%type<ast> printParameterRest
%type<ast> printParameter

%left '|'
%left '<' '>' OPERATOR_LE OPERATOR_GE OPERATOR_DIF OPERATOR_EQ
%left '+' '-'
%left '*' '/' '^'

%%

programa: declarationList  { astRoot = $$; check_and_set_declarations(astRoot); check_undeclared(); check_commands_type(astRoot);}
	;

declarationList: declaration ';' declarationList  { $$ = astCreate(AST_DCL_LIST, 0, $1, $3, 0, 0); }
	|                                         { $$ = 0; }
	;

declaration: TK_IDENTIFIER '=' varType ':' literal			  { $$ = astCreate(AST_DCL_VAR, $1, $3, $5, 0, 0); }
	| TK_IDENTIFIER '=' varType '[' LIT_INTEGER ']' ':' literalList   { $$ = astCreate(AST_DCL_VEC_VALUES, $1, $3, astCreate(AST_SYMBOL, $5, 0, 0, 0, 0), $8, 0); }
	| TK_IDENTIFIER '=' varType '[' LIT_INTEGER ']'                   { $$ = astCreate(AST_DCL_VEC, $1, $3, astCreate(AST_SYMBOL, $5, 0, 0, 0, 0), 0, 0); }
	| TK_IDENTIFIER '(' parameterList ')' '=' varType commandBlock    { $$ = astCreate(AST_DCL_FUN, $1, $3, $6, $7, 0); }
	;

commandBlock: '{' commandList '}'			{ $$ = astCreate(AST_CMD_BLOCK, 0, $2, 0, 0, 0); }
	;

commandList: command commandList			{ $$ = astCreate(AST_CMD_LIST, 0, $1, $2, 0, 0); }
	|						{ $$ = 0; }
	;

command: TK_IDENTIFIER '=' expr           					{ $$ = astCreate(AST_ATTR, $1, $3, 0, 0, 0);    }
	| TK_IDENTIFIER '[' expr ']' '=' expr					{ $$ = astCreate(AST_ATTR_VEC, $1, $3, $6, 0, 0);   }
	| KW_READ TK_IDENTIFIER							{ $$ = astCreate(AST_READ, $2, 0, 0, 0, 0);     }
	| KW_PRINT printParameterList						{ $$ = astCreate(AST_PRINT, 0, $2, 0, 0, 0);      }
	| KW_RETURN expr							{ $$ = astCreate(AST_RETURN, 0, $2, 0, 0, 0);      }
	| KW_IF '(' expr ')' KW_THEN command					{ $$ = astCreate(AST_IF, 0, $3, $6, 0, 0);      }
	| KW_IF '(' expr ')' KW_THEN command KW_ELSE command			{ $$ = astCreate(AST_IF_ELSE, 0, $3, $6, $8, 0);     }
	| KW_WHILE '(' expr ')' command						{ $$ = astCreate(AST_WHILE, 0, $3, $5, 0, 0);	}
	| KW_LOOP '(' TK_IDENTIFIER ':' expr ',' expr ',' expr ')' command	{ $$ = astCreate(AST_LOOP, $3, $5, $7,$9, $11); }
 	| commandBlock								{ $$ = $1; }
 	|									{ $$ = 0;  }
	;

expr: literal 						{ $$ = $1; }
	| TK_IDENTIFIER					{ $$ = astCreate(AST_SYMBOL, $1, 0, 0, 0, 0); }
	| TK_IDENTIFIER '[' expr ']'			{ $$ = astCreate(AST_VECTOR, $1, $3, 0, 0, 0);   }
	| TK_IDENTIFIER '(' callParameterList ')'       { $$ = astCreate(AST_FUNCTION, $1, $3, 0, 0, 0);   }
	| expr '+' expr					{ $$ = astCreate(AST_ADD, 0, $1, $3, 0, 0);   }
	| expr '-' expr					{ $$ = astCreate(AST_SUB, 0, $1, $3, 0, 0);   }
	| expr '/' expr                                 { $$ = astCreate(AST_DIV, 0, $1, $3, 0, 0);   }
	| expr '*' expr                                 { $$ = astCreate(AST_MUL, 0, $1, $3, 0, 0);   }
	| expr '^' expr                                 { $$ = astCreate(AST_POW, 0, $1, $3, 0, 0);   }
	| expr '|' expr                                 { $$ = astCreate(AST_OR, 0, $1, $3, 0, 0);    }
	| expr '>' expr                                 { $$ = astCreate(AST_GREATER, 0, $1, $3, 0, 0);   }
	| expr '<' expr                                 { $$ = astCreate(AST_LESS, 0, $1, $3, 0, 0);   }
	| expr OPERATOR_LE expr                         { $$ = astCreate(AST_LESS_EQUAL, 0, $1, $3, 0, 0);    }
	| expr OPERATOR_GE expr                         { $$ = astCreate(AST_GREATER_EQUAL, 0, $1, $3, 0, 0);    }
	| expr OPERATOR_EQ expr                         { $$ = astCreate(AST_EQUAL, 0, $1, $3, 0, 0);    }
	| expr OPERATOR_DIF expr                        { $$ = astCreate(AST_DIFFERENT, 0, $1, $3, 0, 0);   }
	| '(' expr ')'                                  { $$ = astCreate(AST_PARENTHESIS_EXPR, 0, $2, 0, 0, 0); }
	;

callParameterList: expr restCallParater 	{ $$ = astCreate(AST_FUNC_CALL_PARAMETER_LIST, 0, $1, $2, 0, 0);   }
	|					{ $$ = 0; }
	;

restCallParater: ',' expr restCallParater	{ $$ = astCreate(AST_FUNC_CALL_PARAMETER_LIST_REST, 0, $2, $3, 0, 0);   }
	|					{ $$ = 0; }
	;

printParameterList: printParameter printParameterRest   { $$ = astCreate(AST_PRINT_PARAMETER_LIST, 0, $1, $2, 0, 0);   }
	|                                               { $$ = 0; }
	;

printParameterRest: ',' printParameter printParameterRest  { $$ = astCreate(AST_PRINT_PARAMETER_LIST_REST, 0, $2, $3, 0, 0);   }
	|                                                  { $$ = 0; }
	;

printParameter: LIT_STRING				{ $$ = astCreate(AST_SYMBOL, $1, 0, 0, 0, 0);   }
	| expr
	;

literalList: literal literalList                        { $$ = astCreate(AST_LITERAL_LIST, 0, $1, $2, 0, 0);   }
	|                                               { $$ = 0; }
	;

literal: LIT_INTEGER                                    { $$ = astCreate(AST_SYMBOL, $1, 0, 0, 0, 0);   }
	| LIT_FLOAT                                     { $$ = astCreate(AST_SYMBOL, $1, 0, 0, 0, 0);   }
	| LIT_TRUE                                      { $$ = astCreate(AST_SYMBOL, $1, 0, 0, 0, 0);   }
	| LIT_FALSE                                     { $$ = astCreate(AST_SYMBOL, $1, 0, 0, 0, 0);   }
	| LIT_CHAR                                      { $$ = astCreate(AST_SYMBOL, $1, 0, 0, 0, 0);   }
	;

parameterList: TK_IDENTIFIER '=' varType restParameter         { $$ = astCreate(AST_PARAMETER_LIST, $1, $3, $4, 0, 0);   }
	|                                                      { $$ = 0; }
	;

restParameter: ',' TK_IDENTIFIER '=' varType restParameter     { $$ = astCreate(AST_PARAMETER_LIST_REST, $2, $4, $5, 0, 0);   }
	|                                                      { $$ = 0; }
	;

varType:  KW_CHAR			{ $$ = astCreate(AST_TYPE_CHAR, 0, 0, 0, 0, 0);   }
	| KW_INT			{ $$ = astCreate(AST_TYPE_INT, 0, 0, 0, 0, 0);    }
	| KW_FLOAT			{ $$ = astCreate(AST_TYPE_FLOAT, 0, 0, 0, 0, 0);  }
	| KW_BOOL			{ $$ = astCreate(AST_TYPE_BOOL, 0, 0, 0, 0, 0);   }
%%

int yyerror() {
    	fprintf(stderr, "Syntax error at line %i. \n", getLineNumber());
	exit(3);
}

AST* getAST(){
	return astRoot;
}