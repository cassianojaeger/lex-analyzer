/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/
#include <stdio.h>
#include <stdlib.h>
#include "ast.h"

int level = 0;
void ident(FILE *f);

AST *astCreate(int type, HASH_NODE *symbol, AST* s0, AST* s1, AST* s2, AST* s3)
{
    AST *newNode = (AST*) calloc(1, sizeof(AST));
    newNode->type = type;
    newNode->symbol = symbol;
    newNode->sons[0] = s0;
    newNode->sons[1] = s1;
    newNode->sons[2] = s2;
    newNode->sons[3] = s3;

    return newNode;
}

void astPrint(AST *node, int level)
{
    if(node == 0)
        return;

    for(int i=0; i< level; ++i)
    {
        fprintf(stderr, "  ");
    }

    fprintf(stderr, "ast(");
    switch (node->type)
    {
        case AST_SYMBOL: fprintf(stderr, "AST_SYMBOL"); break;
        case AST_ADD: fprintf(stderr, "AST_ADD"); break;
        case AST_SUB: fprintf(stderr, "AST_SUB"); break;
        case AST_DIV: fprintf(stderr, "AST_DIV"); break;
        case AST_MUL: fprintf(stderr, "AST_MUL"); break;
        case AST_POW: fprintf(stderr, "AST_POW"); break;
        case AST_OR: fprintf(stderr, "AST_OR"); break;
        case AST_GREATER: fprintf(stderr, "AST_GREATER"); break;
        case AST_LESS: fprintf(stderr, "AST_LESS"); break;
        case AST_LESS_EQUAL: fprintf(stderr, "AST_LESS_EQUAL"); break;
        case AST_GREATER_EQUAL: fprintf(stderr, "AST_GREATER_EQUAL"); break;
        case AST_EQUAL: fprintf(stderr, "AST_EQUAL"); break;
        case AST_DIFFERENT: fprintf(stderr, "AST_DIFFERENT"); break;
        case AST_VECTOR: fprintf(stderr, "AST_VECTOR"); break;
        case AST_FUNCTION: fprintf(stderr, "AST_FUNCTION"); break;
        case AST_ATTR: fprintf(stderr, "AST_ATTR"); break;
        case AST_FUNC_CALL_PARAMETER_LIST: fprintf(stderr, "AST_FUNC_CALL_PARAMETER_LIST"); break;
        case AST_CP: fprintf(stderr, "AST_CP"); break;
        case AST_ATTR_VEC: fprintf(stderr, "AST_ATTR_VEC"); break;
        case AST_READ: fprintf(stderr, "AST_READ"); break;
        case AST_RETURN: fprintf(stderr, "AST_RETURN"); break;
        case AST_IF: fprintf(stderr, "AST_IF"); break;
        case AST_WHILE: fprintf(stderr, "AST_WHILE"); break;
        case AST_LOOP: fprintf(stderr, "AST_LOOP"); break;
        case AST_PRINT: fprintf(stderr, "AST_PRINT"); break;
        case AST_PRINT_PARAMETER_LIST: fprintf(stderr, "AST_PRINT_PARAMETER_LIST"); break;
        case AST_CMD_LIST: fprintf(stderr, "AST_CMD_LIST"); break;
        case AST_CMD_BLOCK: fprintf(stderr, "AST_CMD_BLOCK"); break;
        case AST_TYPE_CHAR: fprintf(stderr, "AST_TYPE_CHAR"); break;
        case AST_TYPE_INT: fprintf(stderr, "AST_TYPE_INT"); break;
        case AST_TYPE_FLOAT: fprintf(stderr, "AST_TYPE_FLOAT"); break;
        case AST_TYPE_BOOL: fprintf(stderr, "AST_TYPE_BOOL"); break;
        case AST_DCL_VAR: fprintf(stderr, "AST_DCL_VAR"); break;
        case AST_DCL_VEC: fprintf(stderr, "AST_DCL_VEC"); break;
        case AST_LITERAL_LIST: fprintf(stderr, "AST_LITERAL_LIST"); break;
        case AST_DCL_FUN: fprintf(stderr, "AST_DCL_FUN"); break;
        case AST_PARAMETER_LIST: fprintf(stderr, "AST_PARAMETER_LIST"); break;
        case AST_PMT: fprintf(stderr, "AST_PMT"); break;
        case AST_DCL_LIST: fprintf(stderr, "AST_DCL_LIST"); break;
        case AST_DCL_VEC_VALUES: fprintf(stderr, "AST_DCL_VEC_VALUES"); break;
        case AST_PRINT_PARAMETER_LIST_REST: fprintf(stderr, "AST_PRINT_PARAMETER_LIST_REST"); break;
        case AST_IF_ELSE: fprintf(stderr, "AST_IF_ELSE"); break;
        case AST_FUNC_CALL_PARAMETER_LIST_REST: fprintf(stderr, "AST_FUNC_CALL_PARAMETER_LIST_REST"); break;
        case AST_PARENTHESIS_EXPR: fprintf(stderr, "AST_PARENTHESIS_EXPR"); break;
        case AST_PARAMETER_LIST_REST: fprintf(stderr, "AST_PARAMETER_LIST_REST"); break;

        default: fprintf(stderr, "AST_UNKNOWN"); break;
    }

    if(node->symbol != 0)
    {
        fprintf(stderr, ",%s\n", node->symbol->text);
    }
    else
    {
        fprintf(stderr, ",0,\n");
    }

    for (int i = 0; i < MAX_SONS; ++i)
    {
        astPrint(node->sons[i], level+1);
    }

    for(int i=0; i< level; ++i)
    {
        fprintf(stderr, "  ");
    }
    fprintf(stderr, ")\n");
}

void astToFile(AST* node, FILE *f)
{
    int i = 0;


    if(node == 0)
        return;

    switch(node->type)
    {
        case AST_SYMBOL: {
            fprintf(f, " %s", node->symbol->text);
            break;
        }
        case AST_TYPE_INT: {
            fprintf(f, "int");
            break;
        }
        case AST_TYPE_CHAR: {
            fprintf(f, "char");
            break;
        }
        case AST_TYPE_FLOAT: {
            fprintf(f, "float");
            break;
        }
        case AST_TYPE_BOOL: {
            fprintf(f, "bool");
            break;
        }
        case AST_DCL_LIST:
        case AST_LITERAL_LIST:
        case AST_CMD_LIST:
        case AST_PRINT_PARAMETER_LIST:
        case AST_FUNC_CALL_PARAMETER_LIST: {
            for(i = 0; i < MAX_SONS; i++)
                astToFile(node->sons[i], f);
            break;
        }
        case AST_DCL_VAR: {
            fprintf(f, "%s",node->symbol->text);
            fprintf(f, " = ");

            astToFile(node->sons[0], f);
            fprintf(f, " :");

            astToFile(node->sons[1], f);
            fprintf(f, ";\n");
            break;
        }
        case AST_DCL_VEC_VALUES: {
            fprintf(f, "%s",node->symbol->text);
            fprintf(f, " = ");

            astToFile(node->sons[0], f);
            fprintf(f, " [");
            astToFile(node->sons[1], f);
            fprintf(f, "]");
            fprintf(f, " :");

            astToFile(node->sons[2], f);
            fprintf(f, ";\n");
            break;
        }
        case AST_DCL_VEC: {
            fprintf(f, "%s",node->symbol->text);
            fprintf(f, " = ");

            astToFile(node->sons[0], f);
            fprintf(f, " [");
            astToFile(node->sons[1], f);
            fprintf(f, "]");
            fprintf(f, ";\n");
            break;
        }
        case AST_DCL_FUN: {
            fprintf(f, "\n");
            fprintf(f, "%s",node->symbol->text);

            fprintf(f, " (");
            astToFile(node->sons[0], f);
            fprintf(f, ")");

            fprintf(f, " = ");

            astToFile(node->sons[1], f);
            astToFile(node->sons[2], f);
            fprintf(f, ";\n");
            break;
        }
        case AST_CMD_BLOCK: {
            fprintf(f, " {");
            level++;
            astToFile(node->sons[0], f);
            fprintf(f, "\n");
            level--;
            ident(f);
            fprintf(f, "}");
            break;
        }
        case AST_ATTR: {
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "%s",node->symbol->text);
            fprintf(f, " =");
            astToFile(node->sons[0], f);
            break;
        }
        case AST_ATTR_VEC: {
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "%s",node->symbol->text);
            fprintf(f, " [");
            astToFile(node->sons[0], f);
            fprintf(f, "]");
            fprintf(f, " =");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_PRINT: {
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "print ");
            astToFile(node->sons[0], f);
            break;
        }
        case AST_READ: {
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "read ");
            fprintf(f, "%s",node->symbol->text);
            break;
        }
        case AST_RETURN: {
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "return");
            astToFile(node->sons[0], f);
            break;
        }
        case AST_IF: {
            fprintf(f, "\n");
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "if ");
            fprintf(f, "(");
            astToFile(node->sons[0], f);
            fprintf(f, " )");
            fprintf(f, " then");
            level++;
            astToFile(node->sons[1], f);
            level--;
            break;
        }
        case AST_IF_ELSE: {
            fprintf(f, "\n");
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "if ");
            fprintf(f, "(");
            astToFile(node->sons[0], f);
            fprintf(f, " )");
            fprintf(f, " then");
            astToFile(node->sons[1], f);
            fprintf(f, " else");
            astToFile(node->sons[2], f);
            break;
        }
        case AST_WHILE: {
            fprintf(f, "\n");
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "while ");
            fprintf(f, "(");
            astToFile(node->sons[0], f);
            fprintf(f, " )");
            level++;
            astToFile(node->sons[1], f);
            level--;
            break;
        }
        case AST_LOOP: {
            fprintf(f, "\n");
            fprintf(f, "\n");
            ident(f);
            fprintf(f, "loop ");
            fprintf(f, "(");
            fprintf(f, "%s",node->symbol->text);
            fprintf(f, " :");
            astToFile(node->sons[0], f);
            fprintf(f, ",");
            astToFile(node->sons[1], f);
            fprintf(f, ",");
            astToFile(node->sons[2], f);
            fprintf(f, ")");
            level++;
            astToFile(node->sons[3], f);
            level--;
            break;
        }
        case AST_VECTOR: {
            fprintf(f, "%s",node->symbol->text);
            fprintf(f, "[");
            astToFile(node->sons[0], f);
            fprintf(f, "]");
            break;
        }
        case AST_FUNCTION: {
            fprintf(f, " %s",node->symbol->text);
            fprintf(f, "(");
            astToFile(node->sons[0], f);
            fprintf(f, ")");
            break;
        }
        case AST_ADD: {
            astToFile(node->sons[0], f);
            fprintf(f, " +");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_SUB: {
            astToFile(node->sons[0], f);
            fprintf(f, " -");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_DIV: {
            astToFile(node->sons[0], f);
            fprintf(f, " /");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_MUL: {
            astToFile(node->sons[0], f);
            fprintf(f, " *");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_POW: {
            astToFile(node->sons[0], f);
            fprintf(f, " ^");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_OR: {
            astToFile(node->sons[0], f);
            fprintf(f, " |");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_GREATER: {
            astToFile(node->sons[0], f);
            fprintf(f, " >");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_LESS: {
            astToFile(node->sons[0], f);
            fprintf(f, " <");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_LESS_EQUAL: {
            astToFile(node->sons[0], f);
            fprintf(f, " <=");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_GREATER_EQUAL: {
            astToFile(node->sons[0], f);
            fprintf(f, " >=");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_EQUAL: {
            astToFile(node->sons[0], f);
            fprintf(f, " ==");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_DIFFERENT: {
            astToFile(node->sons[0], f);
            fprintf(f, " !=");
            astToFile(node->sons[1], f);
            break;
        }
        case AST_PARENTHESIS_EXPR: {
            fprintf(f, " (");
            astToFile(node->sons[0], f);
            fprintf(f, " )");
            break;
        }
        case AST_PRINT_PARAMETER_LIST_REST:
        case AST_FUNC_CALL_PARAMETER_LIST_REST: {
            fprintf(f, ",");
            astToFile(node->sons[0], f);
            astToFile(node->sons[1], f);
            break;
        }
        case AST_PARAMETER_LIST: {
            fprintf(f, " %s",node->symbol->text);
            fprintf(f, "=");
            astToFile(node->sons[0], f);
            astToFile(node->sons[1], f);
        }
        case AST_PARAMETER_LIST_REST: {
            fprintf(f, ",");
            fprintf(f, " %s",node->symbol->text);
            fprintf(f, "=");
            astToFile(node->sons[0], f);
            astToFile(node->sons[1], f);
        }
    }
}

void ident(FILE *f)
{
    for (int i = 0; i < level; i++)
        fprintf(f, "    ");
}