/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef HASH_HEADER
#define HASH_HEADER

#define SYMBOL_TK_IDENTIFIER 1
#define SYMBOL_LIT_INTEGER 2
#define SYMBOL_LIT_FLOAT 3
#define SYMBOL_LIT_BOOL 4
#define SYMBOL_LIT_CHAR 6
#define SYMBOL_LIT_STRING 7

#define SYMBOL_VARIABLE 8
#define SYMBOL_FUNCTION 9
#define SYMBOL_VECTOR 10

#define DATA_TYPE_INT 11
#define DATA_TYPE_BOOL 12
#define DATA_TYPE_FLOAT 13
#define DATA_TYPE_CHAR 14
#define DATA_TYPE_STRING 15

#define HASH_SIZE 997

typedef struct hash_node
{
    int datatype;
    int type;
    char *text;
    struct hash_node *next;
    struct funcParameters *parameter;
    int quantityFuncParameters;
} HASH_NODE;

typedef struct funcParameters
{
    int datatype;
    HASH_NODE *identifier;
    struct funcParameters *next;
} FUNC_PARAMETER;

void hashInit (void);
int hashAddress(char *text);
HASH_NODE* hashInsert(char *text, int type);
HASH_NODE* hashFind(char *text);
void hashPrint(void);
int hash_check_undeclared(void);
void hash_insert_func_parameter(FUNC_PARAMETER *newParameter, HASH_NODE *func);

#endif