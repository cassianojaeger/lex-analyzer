/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/
#include "hash.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

HASH_NODE*Table[HASH_SIZE];

void hashInit(void){
    int tableIndex;
    for(tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        Table[tableIndex]=0;
}

int hashAddress(char *text){
    int address = 1;

    for(int characterIndex=0; characterIndex<strlen(text); characterIndex++)
        address = (address * text[characterIndex]) % HASH_SIZE + 1;

    return address - 1;
}

HASH_NODE* hashFind(char *text){
    HASH_NODE *node;

    int address = hashAddress(text);
    for(node=Table[address]; node; node = node->next)
        if(strcmp(node->text, text) == 0)
            return node;

    return 0;
}

HASH_NODE *hashInsert(char *text, int type){
    HASH_NODE *newNode;
    int address = hashAddress(text);

    if((newNode = hashFind(text)) != 0)
        return newNode;

    newNode = (HASH_NODE*) calloc(1, sizeof(HASH_NODE));
    newNode->text = (char*) calloc(strlen(text)+1, sizeof(char));
    newNode->type = type;
    newNode->quantityFuncParameters = 0;
    strcpy(newNode->text, text);

    newNode->next = Table[address];
    Table[address] = newNode;

    return newNode;
}

void hashPrint(void){
    HASH_NODE *node;
    FUNC_PARAMETER *parameter;

    for(int tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        for(node=Table[tableIndex]; node; node = node->next) {
            fprintf(stderr, "\nTable[%d] has %s with SYMBOL_%i and DATA_TYPE_%i with %i parameters", tableIndex, node->text, node->type,
                   node->datatype, node->quantityFuncParameters);

            for (parameter = node->parameter; parameter; parameter = parameter->next)
                fprintf(stderr, "\n        %s = DATA_TYPE_%i", parameter->identifier->text, parameter->datatype);
        }
}

int hash_check_undeclared(void)
{
    int undeclared = 0;
    HASH_NODE *node;

    for(int tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        for(node=Table[tableIndex]; node; node = node->next)
             if(node->type == SYMBOL_TK_IDENTIFIER)
             {
                 fprintf(stderr, "SEMANTIC ERROR: identifier %s undeclared \n", node->text);
                 ++undeclared;
             }

    return undeclared;
}

void hash_insert_func_parameter(FUNC_PARAMETER *newParameter, HASH_NODE *func)
{
    if(!func->parameter)
    {
        func->parameter = newParameter;
    }
    else
    {
        FUNC_PARAMETER *currentParameter = func->parameter;
        while(currentParameter->next)
        {
            currentParameter = currentParameter->next;
        }
        currentParameter->next = newParameter;
    }

    func->quantityFuncParameters++;
    fprintf(stderr, "\n");
}

HASH_NODE* make_temp(void)
{
    static int serial = 0;
    char buffer[256] = "";

    sprintf(buffer, "q@!xIQlGaPd0__%d", serial++);
    return hashInsert(buffer, SYMBOL_VARIABLE);
}

HASH_NODE* make_label(char prefix[128])
{
    static int serial_label = 0;
    char buffer[128] = "";

    char* completeName;
    completeName = malloc(strlen(buffer)+1+strlen(prefix));
    strcpy(completeName, prefix);
    strcat(completeName, buffer);

    sprintf(buffer, completeName, serial_label++);
    return hashInsert(buffer, SYMBOL_LABEL);
}