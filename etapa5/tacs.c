#include "tacs.h"

TAC* tac_create(int type, HASH_NODE *res, HASH_NODE *op1, HASH_NODE *op2)
{
    TAC *newTac = (TAC*) calloc(1, sizeof(TAC));
    newTac->type = type;
    newTac->result = res;
    newTac->op1 = op1;
    newTac->op2 = op2;
    newTac->next = 0;
    newTac->previous = 0;

    return newTac;
}
void tac_print(TAC *tac)
{
    if(!tac || tac->type == TAC_SYMBOL || tac->type == TAC_BUFFER) return;

    fprintf(stderr, "TAC(");

    switch (tac->type)
    {
        case TAC_ADD: fprintf(stderr, "TAC_ADD"); break;
        case TAC_SUB: fprintf(stderr, "TAC_SUB"); break;
        case TAC_COPY: fprintf(stderr, "TAC_COPY"); break;
        case TAC_DIV: fprintf(stderr, "AST_DIV"); break;
        case TAC_MUL: fprintf(stderr, "TAC_MUL"); break;
        case TAC_POW: fprintf(stderr, "TAC_POW"); break;
        case TAC_OR: fprintf(stderr, "TAC_OR"); break;
        case TAC_GREATER: fprintf(stderr, "TAC_GREATER"); break;
        case TAC_LESS: fprintf(stderr, "TAC_LESS"); break;
        case TAC_LESS_EQUAL: fprintf(stderr, "TAC_LESS_EQUAL"); break;
        case TAC_GREATER_EQUAL: fprintf(stderr, "TAC_GREATER_EQUAL"); break;
        case TAC_EQUAL: fprintf(stderr, "TAC_EQUAL"); break;
        case TAC_DIFFERENT: fprintf(stderr, "TAC_DIFFERENT"); break;
        case TAC_JUMP_FALSE: fprintf(stderr, "TAC_JUMP_FALSE"); break;
        case TAC_JUMP_TRUE: fprintf(stderr, "TAC_JUMP_TRUE"); break;
        case TAC_LOOP_COPY: fprintf(stderr, "TAC_LOOP_COPY"); break;
        case TAC_COMPARE_LOOP: fprintf(stderr, "TAC_COMPARE_LOOP"); break;
        case TAC_JUMP: fprintf(stderr, "TAC_JUMP"); break;
        case TAC_READ: fprintf(stderr, "TAC_READ"); break;
        case TAC_ATTR_VEC: fprintf(stderr, "TAC_ATTR_VEC"); break;
        case TAC_VECTOR: fprintf(stderr, "TAC_VECTOR"); break;
        case TAC_SYMBOL: fprintf(stderr, "TAC_SYMBOL"); break;
        case TAC_FUNCTION: fprintf(stderr, "TAC_FUNCTION"); break;
        case TAC_FUNC_ARG: fprintf(stderr, "TAC_FUNC_ARG"); break;
        case TAC_PRINT: fprintf(stderr, "TAC_PRINT"); break;
        case TAC_BEGIN_FUNC: fprintf(stderr, "TAC_BEGIN_FUNC"); break;
        case TAC_END_FUNC: fprintf(stderr, "TAC_END_FUNC"); break;
        case TAC_VAR_DECL: fprintf(stderr, "TAC_VAR_DECL"); break;
        case TAC_VEC_DECL: fprintf(stderr, "TAC_VEC_DECL"); break;
        case TAC_VEC_VAL_DECL: fprintf(stderr, "TAC_VEC_VAL_DECL"); break;
        case TAC_VEC_VALUES_ASSIGN: fprintf(stderr, "TAC_VEC_VALUES_ASSIGN"); break;
        case TAC_LABEL: fprintf(stderr, "TAC_LABEL"); break;

        default: fprintf(stderr, "TAC_UNKNOWN"); break;
    }

    fprintf(stderr, ",%s", (tac->result) ? tac->result->text:"0");
    fprintf(stderr, ",%s", (tac->op1) ? tac->op1->text:"0");
    fprintf(stderr, ",%s", (tac->op2) ? tac->op2->text:"0");
    fprintf(stderr, "); \n");
}
void tac_print_backwards(TAC *tac)
{
    if(!tac) return;
    else
    {
        tac_print_backwards(tac->previous);
        tac_print(tac);
    }
}

TAC* generate_code(AST *node)
{
    TAC *result = 0;
    TAC *code[MAX_SONS];

    if(!node) return 0;

    for(int i = 0; i<MAX_SONS; i++)
        code[i] = generate_code(node->sons[i]);

    switch (node->type)
    {
        //types
        case AST_TYPE_INT:
        case AST_TYPE_BOOL:
        case AST_TYPE_FLOAT:
        case AST_TYPE_CHAR: result = tac_create(TAC_SYMBOL, 0, 0, 0); break;

        // variables
        case AST_SYMBOL: result = tac_create(TAC_SYMBOL, node->symbol, 0, 0); break;
        case AST_VECTOR: result = tac_create(TAC_VECTOR, node->symbol, code[0]->result, 0); break;
        case AST_FUNCTION: result = make_func(code, node); break;

        // expr
        case AST_ADD: result = create_binary_expr(code, TAC_ADD); break;
        case AST_SUB: result = create_binary_expr(code, TAC_SUB); break;
        case AST_DIV: result = create_binary_expr(code, TAC_DIV); break;
        case AST_MUL: result = create_binary_expr(code, TAC_MUL); break;
        case AST_POW: result = create_binary_expr(code, TAC_POW); break;
        case AST_OR: result = create_binary_expr(code, TAC_OR); break;
        case AST_GREATER: result = create_binary_expr(code, TAC_GREATER); break;
        case AST_GREATER_EQUAL: result = create_binary_expr(code, TAC_GREATER_EQUAL); break;
        case AST_LESS: result = create_binary_expr(code, TAC_LESS); break;
        case AST_LESS_EQUAL: result = create_binary_expr(code, TAC_LESS_EQUAL); break;
        case AST_EQUAL: result = create_binary_expr(code, TAC_EQUAL); break;
        case AST_DIFFERENT: result = create_binary_expr(code, TAC_DIFFERENT); break;

        //command
        case AST_ATTR: result = tac_join(code[0], tac_create(TAC_COPY, node->symbol, code[0]->result, 0)); break;
        case AST_ATTR_VEC: result = tac_join(code[0], tac_join(code[1], tac_create(TAC_ATTR_VEC, node->symbol, code[0]->result, code[1]->result))); break;
        case AST_READ: result = tac_join(code[0], tac_create(TAC_READ, 0, node->symbol, 0)); break;
        case AST_IF: result = make_if(code[0], code[1]); break;
        case AST_IF_ELSE: result = make_if_else(code); break;
        case AST_LOOP: result = make_loop(code, node); break;
        case AST_WHILE: result = make_while(code, node); break;
        case AST_PRINT: result = make_print(code, node); break;

        //decl
        case AST_DCL_FUN: result = make_func_decl(code, node); break;
        case AST_DCL_VAR: result = make_var_decl(code, node); break;
        case AST_DCL_VEC: result = make_vec_decl(code, node); break;
        case AST_DCL_VEC_VALUES: result = make_vec_val_decl(code, node); break;

        default: result = tac_join(code[0], tac_join(code[1], tac_join(code[2], code[3]))); break;
    }

    return result;
}

TAC* make_print(TAC *code[MAX_SONS], AST *node)
{
    int count = 0;
    char *countStr = malloc(256);

    TAC *firstJoin = code[0];
    node = node->sons[0];

    while(node)
    {
        count++;
        sprintf(countStr, "%d", count);
        HASH_NODE* countNode = hashInsert(countStr, SYMBOL_SCALAR);

        TAC *expr = generate_code(node->sons[0]);

        TAC *valuesTac = tac_create(TAC_PRINT, expr->result, expr->op1, countNode);
        firstJoin = tac_join(valuesTac, firstJoin);

        node = node->sons[1];
    }

    return firstJoin;
}

TAC* make_vec_val_decl(TAC *code[MAX_SONS], AST *node)
{
    int count = 0;
    char *countStr = malloc(256);

    TAC *firstJoin = tac_join(code[0], tac_create(TAC_VEC_VAL_DECL, node->symbol, code[1]->result, 0));
    node = node->sons[2];

    while(node)
    {
        count++;
        sprintf(countStr, "%d", count);
        HASH_NODE* countNode = hashInsert(countStr, SYMBOL_SCALAR);

        TAC *valuesTac = tac_create(TAC_VEC_VALUES_ASSIGN, node->sons[0]->symbol, 0, countNode);
        firstJoin = tac_join(valuesTac, firstJoin);

        node = node->sons[1];
    }

    return firstJoin;
}

TAC* make_vec_decl(TAC *code[MAX_SONS], AST *node)
{
    return tac_join(code[0], tac_create(TAC_VEC_DECL, node->symbol, code[1]->result, 0));
}

TAC* make_var_decl(TAC *code[MAX_SONS], AST *node)
{
    return tac_join(code[0], tac_create(TAC_VAR_DECL, node->symbol, code[1]->result, 0));
}

TAC* make_func_decl(TAC *code[MAX_SONS], AST *node)
{
    int count = 0;
    char *countStr = malloc(256);

    TAC* beginTac;
    TAC* endTac = 0;
    TAC* firstJoin = 0;
    TAC* paramTac = 0;

    beginTac = tac_create(TAC_BEGIN_FUNC, node->symbol, 0, 0);
    endTac = tac_create(TAC_END_FUNC, code[2]->result, 0, 0);

    firstJoin = tac_join(beginTac, tac_join(code[0], tac_join(code[2], endTac)));

    node = node->sons[0];

    while(node) {
        count++;
        sprintf(countStr, "%d", count);
        HASH_NODE* countNode = hashInsert(countStr, SYMBOL_SCALAR);

        paramTac = tac_create(TAC_FUNC_ARG, node->symbol, code[1]->result, countNode);
        tac_join(paramTac, firstJoin);

        node = node->sons[1];
    }

    return firstJoin;
}

TAC* make_func(TAC *code[MAX_SONS], AST *node)
{
    int count = 0;
    char *countStr = malloc(256);

    TAC* resultParam;
    TAC* bufferTac = 0;
    TAC* funcallTac = 0;
    TAC* firstJoin = 0;
    TAC* paramTac = 0;

    bufferTac = tac_create(TAC_BUFFER, code[0]?code[0]->result:0, 0, 0);

    bufferTac->result = make_temp();

    funcallTac = tac_create(TAC_FUNCTION, bufferTac->result, node->symbol, 0);
    firstJoin = tac_join(tac_join(code[0],bufferTac), funcallTac);

    node = node->sons[0];
    while(node) {
        count++;
        sprintf(countStr, "%d", count);
        HASH_NODE* countNode = hashInsert(countStr, SYMBOL_SCALAR);

        resultParam = generate_code(node->sons[0]);
        paramTac = tac_create(TAC_FUNC_ARG, resultParam->result, 0, countNode);
        tac_join(paramTac, firstJoin);

        node = node->sons[1];
    }

    return firstJoin;
}

TAC* make_while(TAC *code[MAX_SONS], AST *node)
{
    TAC *jumpTac = 0;
    TAC *afterWhileTac = 0;
    TAC *jumpLoopWhileTac = 0;
    TAC *initLoopWhileTac = 0;
    HASH_NODE *afterWhileLabel = 0;
    HASH_NODE *loopWhileLabel = 0;

    afterWhileLabel = make_label("POST_WHILE_");
    loopWhileLabel = make_label("INIT_WHILE_");

    initLoopWhileTac = tac_create(TAC_LABEL, loopWhileLabel, 0, 0);
    jumpTac = tac_create(TAC_JUMP_FALSE, afterWhileLabel, code[0]->result, 0);
    afterWhileTac = tac_create(TAC_LABEL, afterWhileLabel, 0, 0);
    jumpLoopWhileTac = tac_create(TAC_JUMP, loopWhileLabel, 0, 0);

    return tac_join(initLoopWhileTac, tac_join(code[0], tac_join(jumpTac, tac_join(code[1], tac_join(jumpLoopWhileTac, afterWhileTac)))));
}

TAC* make_loop(TAC *code[MAX_SONS], AST *node)
{
    TAC *loopCopyTac = 0;
    TAC *compareCondition = 0;
    TAC *jumpFalse = 0;
    TAC *labelTac = 0;
    TAC *initLoopTac = 0;

    TAC *incrementTac = 0;
    TAC *goBackTac = 0;

    HASH_NODE *postLoopLabel = 0;
    HASH_NODE *loopInit = 0;

    postLoopLabel = make_label("POST_LOOP_");
    loopInit = make_label("LOOP_INIT_");

    loopCopyTac = tac_join(code[0], tac_create(TAC_LOOP_COPY, node->symbol, code[0]->result, 0));
    initLoopTac = tac_join(loopCopyTac, tac_create(TAC_LABEL, loopInit, 0 ,0));
    compareCondition = tac_join(initLoopTac, tac_create(TAC_COMPARE_LOOP, make_temp(), node->symbol , code[1]->result));
    jumpFalse = tac_join(compareCondition, tac_create(TAC_JUMP_FALSE, postLoopLabel, compareCondition->result , 0));
    incrementTac = tac_join(jumpFalse, tac_join(code[3], tac_join(code[2], tac_create(TAC_ADD, node->symbol, node->symbol, code[2]->result))));
    goBackTac = tac_join(incrementTac, tac_create(TAC_JUMP, loopInit, 0, 0));
    labelTac = tac_join(goBackTac, tac_create(TAC_LABEL, postLoopLabel, 0, 0));

    return labelTac;
}

TAC* make_if_else(TAC *code[MAX_SONS])
{
    TAC *jumpThenTac = 0;
    TAC *jumpElseTac = 0;
    TAC *afterElseLabelTac = 0;
    TAC *elseLabelTac = 0;
    HASH_NODE *postElseLabel = 0;
    HASH_NODE *postThenLabel = 0;

    postElseLabel = make_label("POST_IF_ELSE_");
    postThenLabel = make_label("POST_IF_THEN_");

    jumpThenTac = tac_create(TAC_JUMP_FALSE, postThenLabel, code[0]->result , 0);
    jumpElseTac = tac_create(TAC_JUMP, postElseLabel, 0, 0);
    elseLabelTac = tac_create(TAC_LABEL, postThenLabel, 0, 0);
    afterElseLabelTac = tac_create(TAC_LABEL, postElseLabel, 0, 0);

    return tac_join(code[0], tac_join(jumpThenTac, tac_join(code[1], tac_join(jumpElseTac, tac_join(elseLabelTac, tac_join(code[2], afterElseLabelTac))))));
}

TAC* make_if(TAC *code0, TAC *code1)
{
    TAC *jumpTac = 0;
    TAC *labelTac = 0;
    HASH_NODE *newLabel = 0;

    newLabel = make_label("POST_IF_THEN_");

    jumpTac = tac_create(TAC_JUMP_FALSE, newLabel, code0->result , 0);
    jumpTac->previous = code0;
    labelTac = tac_create(TAC_LABEL, newLabel, 0, 0);
    labelTac->previous = code1;

    return tac_join(jumpTac, labelTac);
}

TAC* create_binary_expr(TAC *code[MAX_SONS], int tac_type)
{
    return tac_join(tac_join(code[0], code[1]), tac_create(tac_type, make_temp(), code[0]->result, code[1]->result));
}

TAC* tac_join(TAC *l1, TAC *l2)
{
    TAC *point;

    if(!l1) return l2;
    if(!l2) return l1;

    for(point = l2; point->previous != 0; point = point->previous)
        ;

    point->previous = l1;
    return l2;
}