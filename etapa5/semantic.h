/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/

#ifndef SEMANTIC_HEADER
#define SEMANTIC_HEADER

#include "ast.h"
#include "hash.h"

extern int SemanticErrors;
static int returnType;

void check_and_set_declarations(AST *node);
void check_undeclared();
int getSemanticErrors();
void updateDeclarationType(AST *son, AST *parent);
int check_expr(AST *node);
int check_symbol(HASH_NODE *node);
int check_isNumber(int expr);
int check_isBool(int expr);
void check_commands_type(AST *node);
int areNotCompatibleTypes(int identifierType, int exprType);
void check_return_type(AST *node, int funcType);
void updateParameterList(AST *son, HASH_NODE *func);

#endif