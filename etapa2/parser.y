%{
/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 25/09/2020
*/
	#include <stdio.h>
	#include <stdlib.h>
	int yylex();
	int yyerror();
	int getLineNumber(void);
%}

%token KW_CHAR
%token KW_INT
%token KW_FLOAT
%token KW_BOOL

%token KW_IF
%token KW_THEN
%token KW_ELSE
%token KW_WHILE
%token KW_LOOP
%token KW_READ
%token KW_PRINT
%token KW_RETURN

%token OPERATOR_LE
%token OPERATOR_GE
%token OPERATOR_EQ
%token OPERATOR_DIF
%token TK_IDENTIFIER

%token LIT_INTEGER
%token LIT_FLOAT
%token LIT_TRUE
%token LIT_FALSE
%token LIT_CHAR
%token LIT_STRING

%left '|'
%left '<' '>' OPERATOR_LE OPERATOR_GE OPERATOR_DIF OPERATOR_EQ
%left '+' '-'
%left '*' '/' '^'

%%

programa: declarationList
	;

declarationList: declaration ';' declarationList
	|
	;

declaration: TK_IDENTIFIER '=' varType ':' literal
	| TK_IDENTIFIER '=' varType '[' LIT_INTEGER ']' ':' literalList
	| TK_IDENTIFIER '=' varType '[' LIT_INTEGER ']'
	| TK_IDENTIFIER '(' parameterList ')' '=' varType commandBlock
	;

parameterList: parameter restParameter
	|
	;

restParameter: ',' parameter restParameter
	|
	;

parameter: TK_IDENTIFIER '=' varType
	;

varType: KW_CHAR
	| KW_INT
	| KW_FLOAT
	| KW_BOOL

literalList: literal literalList
	|
	;

literal: LIT_INTEGER
	| LIT_FLOAT
	| LIT_TRUE
	| LIT_FALSE
	| LIT_CHAR
	;

commandBlock: '{' commandList '}'
	;

commandList: command commandList
	|
	;

command: TK_IDENTIFIER '=' expr
	| TK_IDENTIFIER '[' expr ']' '=' expr
	| KW_READ TK_IDENTIFIER
	| KW_PRINT printParameterList
	| KW_RETURN expr
	| KW_IF '(' expr ')' KW_THEN command
	| KW_IF '(' expr ')' KW_THEN command KW_ELSE command
	| KW_WHILE '(' expr ')' command
	| KW_LOOP '(' TK_IDENTIFIER ':' expr ',' expr ',' expr ')' command
 	| commandBlock
 	|
	;

printParameterList: printParameter printParameterRest
	|
	;

printParameterRest: ',' printParameter printParameterRest
	|
	;

printParameter: LIT_STRING
	| expr

expr: LIT_INTEGER
	| LIT_FLOAT
	| LIT_TRUE
	| LIT_FALSE
	| LIT_CHAR
	| TK_IDENTIFIER
	| TK_IDENTIFIER '[' expr ']'
	| TK_IDENTIFIER '(' callParameterList ')'
	| expr '+' expr
	| expr '-' expr
	| expr '/' expr
	| expr '*' expr
	| expr '^' expr
	| expr '|' expr
	| expr '>' expr
	| expr '<' expr
	| expr OPERATOR_LE expr
	| expr OPERATOR_GE expr
	| expr OPERATOR_EQ expr
	| expr OPERATOR_DIF expr
	| '(' expr ')'
	;

callParameterList: callParameter restCallParater
	|
	;

restCallParater: ',' callParameter restCallParater
	|
	;

callParameter: expr
	;

%%

int yyerror() {
    fprintf(stderr, "Syntax error at line %i. \n", getLineNumber());
    exit(3);
}