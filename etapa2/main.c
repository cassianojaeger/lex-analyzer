/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 25/09/2020
*/
#include <stdio.h>
#include <stdlib.h>

int isRunning(void);
void initMe(void);

int main(int argc, char** argv)
{
    int token;

    fprintf(stderr,"Rodando main do prof. \n");

    if (argc < 2) {
        printf("call: ./etapa2 input.txt \n");
        exit(1);
    }
    if (0==(yyin = fopen(argv[1],"r"))) {
        printf("Cannot open file %s... \n",argv[1]);
        exit(2);
    }

    initMe();

    yyparse();

    fprintf(stderr,"\nSUCESSO!");
    hashPrint();

    exit(0);
}