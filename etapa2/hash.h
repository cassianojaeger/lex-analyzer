/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 25/09/2020
*/
#ifndef HASH_HEADER
#define HASH_HEADER

#define SYMBOL_TK_IDENTIFIER 1
#define SYMBOL_LIT_INTEGER 2
#define SYMBOL_LIT_FLOAT 3
#define SYMBOL_LIT_TRUE 4
#define SYMBOL_LIT_FALSE 5
#define SYMBOL_LIT_CHAR 6
#define SYMBOL_LIT_STRING 7

#define HASH_SIZE 997

typedef struct hash_node
{
    int type;
    char *text;
    struct hash_node *next;

} HASH_NODE;

void hashInit (void);
int hashAddress(char *text);
HASH_NODE* hashInsert(char *text, int type);
HASH_NODE* hashFind(char *text);
void hashPrint(void);

#endif