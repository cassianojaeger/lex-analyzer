%{
/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 25/09/2020
*/
#include "tokens.h"
#include "hash.h"
int running;
int lineNumber;
%}

%x COMMENT

%%
"int"                            { return KW_INT;                                          }
"char"                           { return KW_CHAR;                                         }
"float"                          { return KW_FLOAT;                                        }
"bool"                           { return KW_BOOL;                                         }
"if"                             { return KW_IF;                                           }
"then"                           { return KW_THEN;                                         }
"else"                           { return KW_ELSE;                                         }
"while"                          { return KW_WHILE;                                        }
"loop"                           { return KW_LOOP;                                         }
"read"                           { return KW_READ;                                         }
"print"                          { return KW_PRINT;                                        }
"return"                         { return KW_RETURN;                                       }
"<="                             { return OPERATOR_LE;                                     }
">="                             { return OPERATOR_GE;                                     }
"=="                             { return OPERATOR_EQ;                                     }
"!="                             { return OPERATOR_DIF;                                    }
"TRUE"                           { hashInsert(yytext, LIT_TRUE);      return LIT_TRUE;     }
"FALSE"                          { hashInsert(yytext, LIT_FALSE);     return LIT_FALSE;    }

[,;:()\[\]\{\}\+\-\*/<>=|^~&$#]  { return yytext[0];                                       }
[A-Za-z|@]([A-Za-z]|[0-9]|_|@)*  { hashInsert(yytext, TK_IDENTIFIER); return TK_IDENTIFIER;}
[0-9][A-F0-9]*                   { hashInsert(yytext, LIT_INTEGER);   return LIT_INTEGER;  }
([0-9]([A-F0-9])*)*\.[0-9A-F]+   { hashInsert(yytext, LIT_FLOAT);     return LIT_FLOAT;    }

[ \t]                            {                                                         }
"\n"                             { ++lineNumber;                                           }
'.'                              { hashInsert(yytext, LIT_CHAR);      return LIT_CHAR;     }
\"(\\.|[^"])*\"                  { hashInsert(yytext, LIT_STRING);    return LIT_STRING;   }

"//".*                           {                                                         }
"/*"                             { BEGIN(COMMENT);                                         }
.                                { return TOKEN_ERROR;                                     }

<COMMENT>.                       {                                                         }
<COMMENT>"\n"                    { ++lineNumber;                                           }
<COMMENT>"*/"                    { BEGIN(INITIAL);                                         }
%%

#include "main.c"
int isRunning(){
    return running;
}

int getLineNumber(){
    return lineNumber;
}

int yywrap()
{
    running = 0;
    return 1;
}

void initMe(void) {
    running = 1;
    lineNumber = 1;
    hashInit();
}
