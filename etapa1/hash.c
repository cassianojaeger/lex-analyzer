/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 25/09/2020
*/
#include "hash.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

HASH_NODE*Table[HASH_SIZE];

void hashInit(void){
    int tableIndex;
    for(tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        Table[tableIndex]=0;
}

int hashAddress(char *text){
    int address = 1;

    for(int characterIndex=0; characterIndex<strlen(text); characterIndex++)
        address = (address * text[characterIndex]) % HASH_SIZE + 1;

    return address - 1;
}

HASH_NODE* hashFind(char *text){
    HASH_NODE *node;

    int address = hashAddress(text);
    for(node=Table[address]; node; node = node->next)
        if(strcmp(node->text, text) == 0)
            return node;

    return 0;
}

HASH_NODE *hashInsert(char *text, int type){
    HASH_NODE *newNode;
    int address = hashAddress(text);

    if((newNode = hashFind(text)) != 0)
        return newNode;

    newNode = (HASH_NODE*) calloc(1, sizeof(HASH_NODE));
    newNode->text = (char*) calloc(strlen(text)+1, sizeof(char));
    newNode->type = type;
    strcpy(newNode->text, text);

    newNode->next = Table[address];
    Table[address] = newNode;

    return newNode;
}

void hashPrint(void){
    HASH_NODE *node;

    for(int tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        for(node=Table[tableIndex]; node; node = node->next)
            printf("\nTable[%d] has %s with type %i", tableIndex, node->text, node->type);
}
