/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/

#include "semantic.h"
#include "hash.h"

int SemanticErrors = 0;
static int returnType = 0;

void check_and_set_declarations(AST *node)
{
    int i;

    if (node == 0)
        return;

    if (node->symbol)
    {
        if (node->symbol->type != SYMBOL_TK_IDENTIFIER
            && (node->type == AST_DCL_VAR
                || node->type == AST_DCL_FUN
                || node->type == AST_DCL_VEC
                || node->type == AST_DCL_VEC_VALUES)
            )
        {
            fprintf(stderr, "Semantic ERROR: variable %s redeclaration \n", node->symbol->text);
            ++SemanticErrors;
        }

        switch (node->type)
        {
            case AST_DCL_VAR:
            case AST_PARAMETER_LIST:
            case AST_PARAMETER_LIST_REST: node->symbol->type = SYMBOL_VARIABLE; updateDeclarationType(node->sons[0], node); break;
            case AST_DCL_FUN:
            {
                node->symbol->type = SYMBOL_FUNCTION;
                updateDeclarationType(node->sons[1], node);
                updateParameterList(node->sons[0], node->symbol);

                returnType = 0;
                check_return_type(node->sons[2], node->symbol->datatype);

                if(returnType != node->symbol->datatype && returnType != 0)
                {
                    fprintf(stderr, "Semantic ERROR: function %s with type DATA_TYPE_%i as wrong return with type DATA_TYPE_%i \n", node->symbol->text, node->symbol->datatype, returnType);
                    ++SemanticErrors;
                }

                break;
            }
            case AST_DCL_VEC:
            case AST_DCL_VEC_VALUES: node->symbol->type = SYMBOL_VECTOR; updateDeclarationType(node->sons[0], node); break;
            case AST_SYMBOL:
            {
                switch (node->symbol->type)
                {
                    case SYMBOL_LIT_BOOL: node->symbol->datatype = DATA_TYPE_BOOL; break;
                    case SYMBOL_LIT_CHAR: node->symbol->datatype = DATA_TYPE_CHAR; break;
                    case SYMBOL_LIT_FLOAT: node->symbol->datatype = DATA_TYPE_FLOAT; break;
                    case SYMBOL_LIT_INTEGER: node->symbol->datatype = DATA_TYPE_INT; break;
                    case SYMBOL_LIT_STRING: node->symbol->datatype = DATA_TYPE_STRING; break;
                }
            }
        }
    }

    for (i = 0; i < MAX_SONS; ++i)
    {
        check_and_set_declarations(node->sons[i]);
    }
}

void check_commands_type(AST *node)
{
    int i;

    if (node == 0)
        return;

    switch (node->type)
    {
        case AST_ATTR: {
            int identifierType = check_symbol(node->symbol);
            int exprType = check_expr(node->sons[0]);

            if(!(check_isNumber(identifierType) && check_isNumber(exprType))
                && !(check_isBool(identifierType) && check_isBool(exprType))
            )
            {
                fprintf(stderr, "Semantic ERROR: variable %s of type DATA_TYPE_%i has wrong assigned type of %i \n", node->symbol->text, identifierType, exprType);
                ++SemanticErrors;
            }
            else if(node->symbol->type != SYMBOL_VARIABLE)
            {
                fprintf(stderr, "Semantic ERROR: identifier %s of type DATA_TYPE_%i is not a variable \n", node->symbol->text, identifierType);
                ++SemanticErrors;
            }
            break;
        }
        case AST_ATTR_VEC: {
            int identifierType = check_symbol(node->symbol);
            int indexType = check_expr(node->sons[0]);
            int exprType = check_expr(node->sons[1]);

            if((check_isBool(indexType) || indexType == DATA_TYPE_FLOAT)
                || (areNotCompatibleTypes(identifierType, exprType)))
            {
                fprintf(stderr, "Semantic ERROR: vector %s of type DATA_TYPE_%i with index DATA_TYPE_%i has assigned type of %i \n", node->symbol->text, identifierType, indexType, exprType);
                ++SemanticErrors;
            }
            else if(node->symbol->type != SYMBOL_VECTOR)
            {
                fprintf(stderr, "Semantic ERROR: identifier %s of type DATA_TYPE_%i is not a vector \n", node->symbol->text, identifierType);
                ++SemanticErrors;
            }
            break;
        }
        case AST_IF:
        case AST_IF_ELSE:
        case AST_WHILE:
        {
            int condition = check_expr(node->sons[0]);

            if(!check_isBool(condition))
            {
                switch (node->type)
                {
                    case AST_IF:
                    case AST_IF_ELSE:
                        fprintf(stderr, "Semantic ERROR: if conditional is not bool \n");
                        break;
                    case AST_WHILE:
                        fprintf(stderr, "Semantic ERROR: while conditional is not bool \n");
                        break;
                }
                ++SemanticErrors;
            }
            break;
        }
        case AST_LOOP:
        {
            int identifierType = check_symbol(node->symbol);
            int identifierExprType = check_expr(node->sons[0]);
            int conditionType = check_expr(node->sons[1]);
            int incrementalType = check_expr(node->sons[2]);

            if(areNotCompatibleTypes(identifierType, identifierExprType) || !check_isNumber(conditionType) || !check_isNumber(incrementalType))
            {
                fprintf(stderr, "Semantic ERROR: loop with (DATA_TYPE_%i : DATA_TYPE_%i; DATA_TYPE_%i ; DATA_TYPE_%i) \n", identifierType, identifierExprType, conditionType, incrementalType);
                ++SemanticErrors;
            }

            break;
        }

        default:
            for (i = 0; i < MAX_SONS; ++i)
                check_commands_type(node->sons[i]);
            break;
    }
}

int areNotCompatibleTypes(int identifierType, int exprType)
{
    return !(check_isNumber(identifierType) && check_isNumber(exprType)) && !(check_isBool(identifierType) && check_isBool(exprType));
}

int check_expr(AST *node)
{
    int i;

    if (node == 0)
        return -1;

    switch (node->type)
    {
        case AST_ADD:
        case AST_SUB:
        case AST_DIV:
        case AST_MUL:
        case AST_POW:
        case AST_OR:
        case AST_GREATER:
        case AST_LESS:
        case AST_LESS_EQUAL:
        case AST_GREATER_EQUAL:
        case AST_EQUAL:
        case AST_DIFFERENT: {
            int leftOperand = check_expr(node->sons[0]);
            int rightOperand = check_expr(node->sons[1]);

            switch (node->type)
            {
                case AST_GREATER:
                case AST_LESS:
                case AST_LESS_EQUAL:
                case AST_GREATER_EQUAL:
                case AST_EQUAL:
                case AST_DIFFERENT:
                {
                    if(check_isNumber(leftOperand) && check_isNumber(rightOperand))
                    {
                        return DATA_TYPE_BOOL;
                    }
                    else
                    {
                        fprintf(stderr, "Semantic ERROR: invalid operands for comparable operation AST_%i. It is < DATA_TYPE_%i OPERATION DATA_TYPE_%i > \n", node->type, leftOperand, rightOperand);
                        ++SemanticErrors;
                    }
                    break;
                }
                case AST_OR:
                {
                    if(check_isBool(leftOperand) && check_isBool(rightOperand))
                    {
                        return DATA_TYPE_BOOL;
                    }
                    else
                    {
                        fprintf(stderr, "Semantic ERROR: invalid operands for logical operation AST_%i (OR). It is < DATA_TYPE_%i OPERATION DATA_TYPE_%i > \n", node->type, leftOperand, rightOperand);
                        ++SemanticErrors;
                    }
                    break;
                }
                default:
                {
                    if(check_isNumber(leftOperand) && check_isNumber(rightOperand))
                    {
                        if(leftOperand == DATA_TYPE_FLOAT || rightOperand == DATA_TYPE_FLOAT)
                            return DATA_TYPE_FLOAT;
                        return DATA_TYPE_INT;
                    }
                    else
                    {
                        fprintf(stderr, "Semantic ERROR: invalid operands for arithmetic operation AST_%i. It is < DATA_TYPE_%i OPERATION DATA_TYPE_%i > \n", node->type, leftOperand, rightOperand);
                        ++SemanticErrors;
                    }
                }
            }
            break;
        }

        case AST_SYMBOL:
            if(node->symbol->datatype != SYMBOL_VARIABLE && node->sons[0])
            {
                fprintf(stderr, "Semantic ERROR: invalid vector %s being accessed as a variable. \n", node->symbol->text);
                ++SemanticErrors;
            }
            else if(node->symbol->type == SYMBOL_FUNCTION)
            {
                fprintf(stderr, "Semantic ERROR: variable %s is not a function. \n", node->symbol->text);
                ++SemanticErrors;
            }
            else
                return check_symbol(node->symbol);
           break;

        case AST_VECTOR: {
            int indexType = check_expr(node->sons[0]);

            if(node->symbol->type != SYMBOL_VECTOR)
            {
                fprintf(stderr, "Semantic ERROR: variable %s IS NOT a vector. \n", node->symbol->text);
                ++SemanticErrors;
            }
            else if(!check_isNumber(indexType))
            {
                fprintf(stderr, "Semantic ERROR: invalid vector index DATA_TYPE_%i. \n", indexType);
                ++SemanticErrors;
            }
            else if (check_isNumber(indexType) && indexType != DATA_TYPE_FLOAT)
                return check_symbol(node->symbol);
            break;
        }

        case AST_FUNCTION:
        {
            if (node->symbol->type != SYMBOL_FUNCTION)
            {
                fprintf(stderr, "Semantic ERROR: variable %s DATA_TYPE_%i is not a function \n", node->symbol->text,node->symbol->datatype);
                ++SemanticErrors;
                break;
            }

            AST *currentNode = node->sons[0];
            FUNC_PARAMETER *currentParameter = node->symbol->parameter;

            for(int i = 1; i<node->symbol->quantityFuncParameters; i++)
            {
                int parameterType = check_expr(currentNode->sons[0]);

                if(parameterType != currentParameter->datatype)
                {
                    fprintf(stderr, "Semantic ERROR: parameter %s should be of DATA_TYPE_%i. Currently it is DATA_TYPE_%i \n", currentParameter->identifier->text, currentParameter->datatype, parameterType);
                    ++SemanticErrors;
                }

                currentNode = currentNode->sons[1];
                if(!currentNode)
                {
                    fprintf(stderr, "Semantic ERROR: function call %s has less arguments than declared \n", node->symbol->text);
                    ++SemanticErrors;
                }

                currentParameter = currentParameter->next;
            }

            if(currentNode && currentNode->sons[1])
            {
                fprintf(stderr, "Semantic ERROR: function call %s has more arguments than declared \n", node->symbol->text);
                ++SemanticErrors;
            }

            return check_symbol(node->symbol);
        }
        case AST_PARENTHESIS_EXPR:
            return check_expr(node->sons[0]);

        default:
            for (i = 0; i < MAX_SONS; ++i)
                check_expr(node->sons[i]);
            break;
    }

    return -1;
}

void updateParameterList(AST *node, HASH_NODE *func)
{
    int i;

    if (node == 0)
        return;

    switch (node->type)
    {
        case AST_PARAMETER_LIST:
        case AST_PARAMETER_LIST_REST:
        {

            if(hashFind(node->symbol->text) && node->symbol->type == SYMBOL_VARIABLE)
            {
                fprintf(stderr, "Semantic ERROR: func parameter %s redeclaration with AST_TYPE_%i\n", node->symbol->text, node->symbol->type);
                ++SemanticErrors;
            }

            FUNC_PARAMETER *newParameter = (FUNC_PARAMETER*) calloc(1, sizeof(FUNC_PARAMETER));
            newParameter->identifier = node->symbol;

            switch (node->sons[0]->type)
            {
                case AST_TYPE_INT: node->symbol->datatype = newParameter->datatype = DATA_TYPE_INT; break;
                case AST_TYPE_CHAR: node->symbol->datatype = newParameter->datatype = DATA_TYPE_CHAR; break;
                case AST_TYPE_FLOAT: node->symbol->datatype = newParameter->datatype = DATA_TYPE_FLOAT; break;
                case AST_TYPE_BOOL: node->symbol->datatype = newParameter->datatype = DATA_TYPE_BOOL; break;
            }

            hash_insert_func_parameter(newParameter, func);
        }
    }

    for (i = 0; i < MAX_SONS; ++i)
        updateParameterList(node->sons[i], func);
}

void updateDeclarationType(AST *son, AST *parent)
{
    if(son)
    {
        switch (son->type)
        {
            case AST_TYPE_INT: parent->symbol->datatype = DATA_TYPE_INT; break;
            case AST_TYPE_CHAR: parent->symbol->datatype = DATA_TYPE_CHAR; break;
            case AST_TYPE_FLOAT: parent->symbol->datatype = DATA_TYPE_FLOAT; break;
            case AST_TYPE_BOOL: parent->symbol->datatype = DATA_TYPE_BOOL; break;
        }
    }
}

void check_return_type(AST *node, int funcType)
{
    int i;

    if (node == 0)
        return;

    switch (node->type)
    {
        case AST_RETURN:
        {
            int internalReturnType = check_expr(node->sons[0]);
            if(check_isNumber(internalReturnType) != check_isNumber(funcType))
            {
                returnType = internalReturnType;
            }
            break;
        }
    }

    for (i = 0; i < MAX_SONS; ++i)
    {
        check_return_type(node->sons[i], funcType);
    }
}

int check_isNumber(int expr)
{
    return expr == DATA_TYPE_INT || expr == DATA_TYPE_FLOAT || expr == DATA_TYPE_CHAR;
}

int check_isBool(int expr)
{
    return expr == DATA_TYPE_BOOL;
}

int check_symbol(HASH_NODE *node)
{
    return node->datatype;
}

void check_undeclared()
{
    SemanticErrors += hash_check_undeclared();
}

int getSemanticErrors()
{
    return SemanticErrors;
}