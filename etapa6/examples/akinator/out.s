## FIXED INIT
	.section	__TEXT,__cstring,cstring_literals
	printintstr: .asciz	"%d\n"
	printstr: .asciz	"%s\n"
	printchar: .asciz	"%c\n"
	readint: .asciz	"%d"

	.section	__TEXT,__text,regular,pure_instructions
## TAC_BEGIN_FUNCTION
	.globl	 _main 
_main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp

## TTAC_LABEL

INIT_WHILE_13:
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_jogar(%rip), %eax
	je		POST_WHILE_12

## TAC_PRINTSTR
	leaq	str_2869(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_3454(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_2613(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_firstQuestion(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_firstQuestion(%rip), %esi
	addq	$16, %rsp

## TAC_EQUAL
	movl	_firstQuestion(%rip), %eax
	cmpl	_1(%rip), %eax
	sete	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__0(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__0(%rip), %eax
	je		POST_IF_THEN_11

## TAC_PRINTSTR
	leaq	str_3169(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_secQuestion(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_secQuestion(%rip), %esi
	addq	$16, %rsp

## TAC_EQUAL
	movl	_secQuestion(%rip), %eax
	cmpl	_1(%rip), %eax
	sete	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__1(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__1(%rip), %eax
	je		POST_IF_THEN_3

## TAC_PRINTSTR
	leaq	str_2749(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_3323(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_retry(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_retry(%rip), %esi
	addq	$16, %rsp

## TAC_DIFFERENT
	movl	_retry(%rip), %eax
	cmpl	_1(%rip), %eax
	setne	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__2(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__2(%rip), %eax
	je		POST_IF_THEN_0

## TAC_COPY
	movl	_FALSE(%rip), %eax
	movl	%eax, _jogar(%rip)

## TTAC_LABEL

POST_IF_THEN_0:
## TTAC_LABEL
	jmp POST_IF_ELSE_2 

## TTAC_LABEL

POST_IF_THEN_3:
## TAC_PRINTSTR
	leaq	str_2142(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1866(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_retry(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_retry(%rip), %esi
	addq	$16, %rsp

## TAC_EQUAL
	movl	_retry(%rip), %eax
	cmpl	_0(%rip), %eax
	sete	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__3(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__3(%rip), %eax
	je		POST_IF_THEN_1

## TAC_COPY
	movl	_FALSE(%rip), %eax
	movl	%eax, _jogar(%rip)

## TTAC_LABEL

POST_IF_THEN_1:
## TTAC_LABEL

POST_IF_ELSE_2:
## TTAC_LABEL
	jmp POST_IF_ELSE_10 

## TTAC_LABEL

POST_IF_THEN_11:
## TAC_PRINTSTR
	leaq	str_1820(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_secQuestion(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_secQuestion(%rip), %esi
	addq	$16, %rsp

## TAC_EQUAL
	movl	_secQuestion(%rip), %eax
	cmpl	_1(%rip), %eax
	sete	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__4(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__4(%rip), %eax
	je		POST_IF_THEN_9

## TAC_PRINTSTR
	leaq	str_2489(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_thirdQuestion(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_thirdQuestion(%rip), %esi
	addq	$16, %rsp

## TAC_EQUAL
	movl	_thirdQuestion(%rip), %eax
	cmpl	_1(%rip), %eax
	sete	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__5(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__5(%rip), %eax
	je		POST_IF_THEN_6

## TAC_PRINTSTR
	leaq	str_2307(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TTAC_LABEL
	jmp POST_IF_ELSE_5 

## TTAC_LABEL

POST_IF_THEN_6:
## TAC_PRINTSTR
	leaq	str_2142(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1866(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_retry(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_retry(%rip), %esi
	addq	$16, %rsp

## TAC_EQUAL
	movl	_retry(%rip), %eax
	cmpl	_0(%rip), %eax
	sete	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__6(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__6(%rip), %eax
	je		POST_IF_THEN_4

## TAC_COPY
	movl	_FALSE(%rip), %eax
	movl	%eax, _jogar(%rip)

## TTAC_LABEL

POST_IF_THEN_4:
## TTAC_LABEL

POST_IF_ELSE_5:
## TTAC_LABEL
	jmp POST_IF_ELSE_8 

## TTAC_LABEL

POST_IF_THEN_9:
## TAC_PRINTSTR
	leaq	str_2142(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1866(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_PRINTSTR
	leaq	str_1332(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_retry(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_retry(%rip), %esi
	addq	$16, %rsp

## TAC_EQUAL
	movl	_retry(%rip), %eax
	cmpl	_0(%rip), %eax
	sete	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__7(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__7(%rip), %eax
	je		POST_IF_THEN_7

## TAC_COPY
	movl	_FALSE(%rip), %eax
	movl	%eax, _jogar(%rip)

## TTAC_LABEL

POST_IF_THEN_7:
## TTAC_LABEL

POST_IF_ELSE_8:
## TTAC_LABEL

POST_IF_ELSE_10:
## TTAC_LABEL
	jmp INIT_WHILE_13 

## TTAC_LABEL

POST_WHILE_12:
## TAC_ENDFUN
	addq	$16, %rsp
	popq	%rbp
	retq
## -- End function 
## DATA SECTION
	.section	__DATA,__data
_FALSE: .long	0
_TRUE: .long	1
s_FALSE: .asciz	"FALSE"
s_TRUE: .asciz	"TRUE"
_qxIQlGaPd0__9: .long	0
_POST_WHILE_26: .long	0
_POST_IF_ELSE_24: .long	0
str_3169: .asciz	"O nome da pessoa que voce esta pensando comeca com a letra C?"
_0: .long	0
_1: .long	1
_qxIQlGaPd0__13: .long	0
_POST_IF_THEN_25: .long	0
_qxIQlGaPd0__4: .long	0
_POST_IF_THEN_11: .long	0
_POST_IF_ELSE_10: .long	0
_qxIQlGaPd0__12: .long	0
_qxIQlGaPd0__11: .long	0
_retry: .long	0
_POST_IF_THEN_6: .long	0
_qxIQlGaPd0__7: .long	0
_POST_IF_ELSE_8: .long	0
_thirdQuestion: .long	0
_POST_IF_ELSE_5: .long	0
_POST_IF_ELSE_2: .long	0
_INIT_WHILE_27: .long	0
_qxIQlGaPd0__10: .long	0
_qxIQlGaPd0__2: .long	0
_POST_IF_THEN_0: .long	0
str_1820: .asciz	"Hmm, essa pessoa existe?"
str_3323: .asciz	"Se desejar continuar jogando, digite 1, caso contrario digite 2."
_POST_IF_THEN_7: .long	0
_POST_IF_ELSE_19: .long	0
_POST_IF_THEN_18: .long	0
_firstQuestion: .long	0
_INIT_WHILE_13: .long	0
_qxIQlGaPd0__5: .long	0
str_2489: .asciz	"O nome dessa pessoa comeca com a letra C?"
_POST_IF_THEN_1: .long	0
_secQuestion: .long	0
_POST_IF_THEN_14: .long	0
_qxIQlGaPd0__0: .long	0
_POST_IF_ELSE_22: .long	0
str_2307: .asciz	"Ah, entao voce esta falando do Cebolinha!"
_POST_IF_THEN_21: .long	0
str_2613: .asciz	"A pessoa que voce esta pensando possui notas boas?"
str_3454: .asciz	"Pense em alguém e eu irei adivinhar. Digite 1 para sim e 0 para nao."
_qxIQlGaPd0__8: .long	0
_POST_IF_ELSE_16: .long	0
str_2142: .asciz	"Desculpe, nao faco a menor ideia..."
_POST_IF_THEN_9: .long	0
_qxIQlGaPd0__3: .long	0
_jogar: .long	1
_POST_WHILE_12: .long	0
_POST_IF_THEN_3: .long	0
_POST_IF_THEN_23: .long	0
str_1332: .asciz	"\n"
_qxIQlGaPd0__6: .long	0
str_1866: .asciz	"Vamos jogar de novo?"
_qxIQlGaPd0__15: .long	0
str_2869: .asciz	"Eu sou akinator, o genio que tudo descobre. Vamos jogar?"
_qxIQlGaPd0__1: .long	0
str_2749: .asciz	"Ah, facil! Voce esta falando do Cassiano J Stradolini."
_POST_IF_THEN_17: .long	0
_POST_IF_THEN_4: .long	0
_POST_IF_THEN_20: .long	0
_qxIQlGaPd0__14: .long	0
_POST_IF_THEN_15: .long	0
