## FIXED INIT
	.section	__TEXT,__cstring,cstring_literals
	printintstr: .asciz	"%d\n"
	printstr: .asciz	"%s\n"
	printchar: .asciz	"%c\n"
	readint: .asciz	"%d"

	.section	__TEXT,__text,regular,pure_instructions
## TAC_BEGIN_FUNCTION
	.globl	 _fibonnaci 
_fibonnaci:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp

## TAC_LOOP_COPY
	movl	_1(%rip), %eax
	movl	%eax, _iterator(%rip)

## TTAC_LABEL

LOOP_INIT_:
## TAC_LESS
	movl	_iterator(%rip), %eax
	cmpl	_nThNumber(%rip), %eax
	setl	%cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movl	%eax, _qxIQlGaPd0__1(%rip)
## TAC_JUMP_FALSE
	movl	$0, %eax
	cmpl	_qxIQlGaPd0__1(%rip), %eax
	je		POST_LOOP_

## TAC_COPY
	movl	_previousNumber(%rip), %eax
	movl	%eax, _ppNumber(%rip)

## TAC_COPY
	movl	_currentNumber(%rip), %eax
	movl	%eax, _previousNumber(%rip)

## TAC_ADD
	movl	_ppNumber(%rip), %ecx
	addl	_previousNumber(%rip), %ecx
	movl	%ecx, _qxIQlGaPd0__0(%rip)

## TAC_COPY
	movl	_qxIQlGaPd0__0(%rip), %eax
	movl	%eax, _currentNumber(%rip)

## TAC_ADD
	movl	_iterator(%rip), %ecx
	addl	_1(%rip), %ecx
	movl	%ecx, _iterator(%rip)

## TTAC_LABEL
	jmp LOOP_INIT_ 

## TTAC_LABEL

POST_LOOP_:
## TAC_ENDFUN
	addq	$16, %rsp
	popq	%rbp
	retq
## -- End function 
## TAC_BEGIN_FUNCTION
	.globl	 _main 
_main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp

## TAC_PRINTSTR
	leaq	string_0(%rip), %rdi
	movb	$0, %al
	callq	_printf

## TAC_READINT
	subq	$16, %rsp
	leaq	readint(%rip), %rdi
	leaq	_numero(%rip), %rsi
	movb	$0, %al
	callq	_scanf
	movl	_numero(%rip), %esi
	addq	$16, %rsp

## TAC_FUNC_ARG_CALL
	movl	_numero(%rip), %eax
	movl	%eax, _nThNumber(%rip)

## TAC_FUNCTION
	callq	_fibonnaci
	movl	_currentNumber(%rip), %eax
	movl	%eax, _qxIQlGaPd0__2(%rip)

## TAC_COPY
	movl	_qxIQlGaPd0__2(%rip), %eax
	movl	%eax, _resultado(%rip)

## TAC_PRINTINT
	leaq	printintstr(%rip), %rdi
	movl	_resultado(%rip), %esi
	movb	$0, %al
	callq	_printf

## TAC_ENDFUN
	addq	$16, %rsp
	popq	%rbp
	retq
## -- End function 
## DATA SECTION
	.section	__DATA,__data
_FALSE: .long	0
_TRUE: .long	1
s_FALSE: .asciz	"FALSE"
s_TRUE: .asciz	"TRUE"
string_0: .asciz	"Qual numero vc quer?"
_0: .long	0
_1: .long	1
_qxIQlGaPd0__4: .long	0
_POST_LOOP_: .long	0
_ppNumber: .long	0
_resultado: .long	0
_nThNumber: .long	0
_qxIQlGaPd0__2: .long	0
_iterator: .long	1
_previousNumber: .long	0
_currentNumber: .long	1
_qxIQlGaPd0__5: .long	0
_numero: .long	0
_qxIQlGaPd0__0: .long	0
_qxIQlGaPd0__3: .long	0
_LOOP_INIT_: .long	0
_qxIQlGaPd0__1: .long	0
