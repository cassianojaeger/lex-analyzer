
#ifndef TACS_HEADER
#define TACS_HEADER

#include "hash.h"
#include "ast.h"

#define TAC_COPY 0
#define TAC_SYMBOL 1
#define TAC_ADD 2
#define TAC_SUB 3
#define TAC_FUNCTION 4
#define TAC_VECTOR 5
#define TAC_DIV 6
#define TAC_MUL 7
#define TAC_POW 8
#define TAC_OR 9
#define TAC_GREATER 10
#define TAC_LESS 11
#define TAC_LESS_EQUAL 12
#define TAC_GREATER_EQUAL 13
#define TAC_EQUAL 14
#define TAC_DIFFERENT 15
#define TAC_JUMP_FALSE 16
#define TAC_LABEL 17
#define TAC_JUMP_TRUE 18
#define TAC_LOOP_COPY 19
#define TAC_COMPARE_LOOP 20
#define TAC_JUMP 21
#define TAC_READ 22
#define TAC_ATTR_VEC 23
#define TAC_FUNC_ARG 24
#define TAC_PRINT 25
#define TAC_BUFFER 26
#define TAC_BEGIN_FUNC 27
#define TAC_END_FUNC 28
#define TAC_VAR_DECL 29
#define TAC_VEC_DECL 30
#define TAC_VEC_VAL_DECL 31
#define TAC_VEC_VALUES_ASSIGN 32
#define TAC_FUNC_ARG_CALL 33
#define TAC_RETURN 34


typedef struct tac_node{
    int type;
    HASH_NODE *result;
    HASH_NODE *op1;
    HASH_NODE *op2;
    struct tac_node *previous;
    struct tac_node *next;
} TAC;

TAC* tac_create(int type, HASH_NODE *res, HASH_NODE *op1, HASH_NODE *op2);
void tac_print(TAC *tac);
void tac_print_backwards(TAC *tac);
TAC* tac_join(TAC *l1, TAC *l2);
TAC* create_binary_expr(TAC *code[MAX_SONS], int tac_type);

// CODE GENERATION

TAC* generate_code(AST *node);
TAC* make_if(TAC *code0, TAC *code1);
TAC* make_if_else(TAC *code[MAX_SONS]);
TAC* make_loop(TAC *code[MAX_SONS], AST *node);
TAC* make_while(TAC *code[MAX_SONS], AST *node);
TAC* make_func(TAC *code[MAX_SONS], AST *node);
TAC* make_args(TAC *code[MAX_SONS], int tacType);
TAC* make_func_decl(TAC *code[MAX_SONS], AST *node);
TAC* make_var_decl(TAC *code[MAX_SONS], AST *node);
TAC* make_vec_decl(TAC *code[MAX_SONS], AST *node);
TAC* make_vec_val_decl(TAC *code[MAX_SONS], AST *node);
TAC* make_print(TAC *code[MAX_SONS], AST *node);



TAC* tacReverse(TAC* tac);
void generateAsm(TAC* first);

TAC* findTacByFuncNameAndParameterOrder(char *funcName, char *paramOrder);
TAC* findReturnTacByFuncName(char *funcName);
TAC* findReturnTac(TAC *tacs);

#endif
