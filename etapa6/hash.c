/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/
#include "hash.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

HASH_NODE*Table[HASH_SIZE];

void hashInit(void){
    int tableIndex;
    for(tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        Table[tableIndex]=0;
}

int hashAddress(char *text){
    int address = 1;

    for(int characterIndex=0; characterIndex<strlen(text); characterIndex++)
        address = (address * text[characterIndex]) % HASH_SIZE + 1;

    return address - 1;
}

HASH_NODE* hashFind(char *text){
    HASH_NODE *node;

    int address = hashAddress(text);
    for(node=Table[address]; node; node = node->next)
        if(strcmp(node->text, text) == 0)
            return node;

    return 0;
}

HASH_NODE *hashInsert(char *text, int type){
    HASH_NODE *newNode;
    int address = hashAddress(text);

    if((newNode = hashFind(text)) != 0)
        return newNode;

    newNode = (HASH_NODE*) calloc(1, sizeof(HASH_NODE));
    newNode->text = (char*) calloc(strlen(text)+1, sizeof(char));
    newNode->type = type;
    newNode->quantityFuncParameters = 0;
    strcpy(newNode->text, text);

    newNode->next = Table[address];
    Table[address] = newNode;

    return newNode;
}

void hashPrint(void){
    HASH_NODE *node;
    FUNC_PARAMETER *parameter;

    for(int tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        for(node=Table[tableIndex]; node; node = node->next) {
            fprintf(stderr, "\nTable[%d] has %s with SYMBOL_%i and DATA_TYPE_%i with %i parameters", tableIndex, node->text, node->type,
                   node->datatype, node->quantityFuncParameters);

            for (parameter = node->parameter; parameter; parameter = parameter->next)
                fprintf(stderr, "\n        %s = DATA_TYPE_%i", parameter->identifier->text, parameter->datatype);
        }
}

int hash_check_undeclared(void)
{
    int undeclared = 0;
    HASH_NODE *node;

    for(int tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        for(node=Table[tableIndex]; node; node = node->next)
             if(node->type == SYMBOL_TK_IDENTIFIER)
             {
                 fprintf(stderr, "SEMANTIC ERROR: identifier %s undeclared \n", node->text);
                 ++undeclared;
             }

    return undeclared;
}

void hash_insert_func_parameter(FUNC_PARAMETER *newParameter, HASH_NODE *func)
{
    if(!func->parameter)
    {
        func->parameter = newParameter;
    }
    else
    {
        FUNC_PARAMETER *currentParameter = func->parameter;
        while(currentParameter->next)
        {
            currentParameter = currentParameter->next;
        }
        currentParameter->next = newParameter;
    }

    func->quantityFuncParameters++;
    fprintf(stderr, "\n");
}

HASH_NODE* make_temp(void)
{
    static int serial = 0;
    char buffer[256] = "";

    sprintf(buffer, "qxIQlGaPd0__%d", serial++);
    return hashInsert(buffer, SYMBOL_VARIABLE);
}

HASH_NODE* make_label(char prefix[128])
{
    static int serial_label = 0;
    char buffer[128] = "";
    sprintf(buffer, "%d", serial_label);

    char* completeName;
    completeName = malloc(strlen(buffer)+1+strlen(prefix));
    strcpy(completeName, prefix);
    strcat(completeName, buffer);

    sprintf(buffer, completeName, serial_label++);
    return hashInsert(buffer, SYMBOL_LABEL);
}

void printAsm(FILE *fout)
{
    HASH_NODE *node;

    fprintf(fout, "\n## DATA SECTION\n"
                  "\t.section\t__DATA,__data\n");

    fprintf(fout, "_%s: .long\t%i\n", "FALSE", 0);
    fprintf(fout, "_%s: .long\t%i\n", "TRUE", 1);

    fprintf(fout, "s_%s: .asciz\t%s\n", "FALSE", "\"FALSE\"");
    fprintf(fout, "s_%s: .asciz\t%s\n", "TRUE", "\"TRUE\"");

    for(int tableIndex=0; tableIndex<HASH_SIZE; tableIndex++)
        for(node=Table[tableIndex]; node; node = node->next) {
            if(node->type != SYMBOL_FUNCTION && node->type != SYMBOL_SCALAR) {
                if (node->type == SYMBOL_LIT_BOOL) {
                    //fprintf(fout, "_%s: .long\t%d\n", node->text, strcmp(node->text, "TRUE") == 0 ? 1 : 0);
                }
                else if(node->type == SYMBOL_LIT_INTEGER) {
                    fprintf(fout, "_%s: .long\t%d\n", node->text, node->value);
                }
                else if(node->type == SYMBOL_LIT_FLOAT) {
                    //TODO: VERIFY FLOAT, CHAR AND STRING
                }
                else if(node->type == SYMBOL_LIT_STRING) {

                    char *stringName = calloc(1, sizeof(char *));

                    int primeira = node->text[0];
                    int segunda = node->text[1];
                    int resultado = ((primeira * strlen(node->text)) + segunda*13);

                    sprintf(stringName, "str_%d", resultado);
                    fprintf(fout, "%s: .asciz\t%s\n", stringName, node->text);
                }
                else if(node->type == SYMBOL_VECTOR) {
                    //TODO: do nothing
                }
                else if(node->type == SYMBOL_LIT_CHAR) {
                    fprintf(fout, "lit_%c: .long\t%d\n", node->text[1], node->text[1]);
                }
                else {
                    //fprintf(stderr, "variable %s with value: %i %i %i\n", node->text, node->value, node->datatype, node->type);
                    fprintf(fout, "_%s: .long\t%i\n", node->text, node->value);
                }
            }
        }
}