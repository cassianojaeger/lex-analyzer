// AST - ABSTRACT SYNTAX TREE
/*
*
* AUTHOR: Cassiano Jaeger Stradolini
* UFRGS CARD NUMBER: 00228497
* DATE: 26/10/2020
*/
#include "hash.h"

#ifndef AST_HEADER

#define AST_HEADER
#define MAX_SONS 4

#define AST_SYMBOL 1
#define AST_ADD 2
#define AST_SUB 3
#define AST_DIV 4
#define AST_MUL 5
#define AST_POW 6
#define AST_OR 7
#define AST_GREATER 8
#define AST_LESS 9
#define AST_LESS_EQUAL 10
#define AST_GREATER_EQUAL 11
#define AST_EQUAL 12
#define AST_DIFFERENT 13
#define AST_CMD_LIST 14
#define AST_VECTOR 15
#define AST_FUNCTION 16
#define AST_ATTR 17
#define AST_FUNC_CALL_PARAMETER_LIST 18
#define AST_CP 20
#define AST_ATTR_VEC 21
#define AST_READ 22
#define AST_RETURN 23
#define AST_IF 24
#define AST_WHILE 25
#define AST_LOOP 26
#define AST_PRINT 27
#define AST_PRINT_PARAMETER_LIST 28
#define AST_CMD_BLOCK 30
#define AST_TYPE_CHAR 31
#define AST_TYPE_INT 32
#define AST_TYPE_FLOAT 33
#define AST_TYPE_BOOL 34
#define AST_DCL_VAR 35
#define AST_DCL_VEC 36
#define AST_LITERAL_LIST 37
#define AST_DCL_FUN 38
#define AST_PARAMETER_LIST 39
#define AST_DCL_VEC_VALUES 40
#define AST_PMT 41
#define AST_DCL_LIST 42
#define AST_PRINT_PARAMETER_LIST_REST 43
#define AST_IF_ELSE 44
#define AST_FUNC_CALL_PARAMETER_LIST_REST 45
#define AST_PARENTHESIS_EXPR 46
#define AST_PARAMETER_LIST_REST 47

typedef struct astnode
{
    int type;
    HASH_NODE *symbol;
    struct astnode *sons[MAX_SONS];
} AST;

AST *astCreate(int type, HASH_NODE *symbol, AST* s0, AST* s1, AST* s2, AST* s3);
void astPrint(AST *node, int level);
void astToFile(AST *node, FILE *fileTree);

#endif
// END OF FILE